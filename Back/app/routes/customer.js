const express = require('express');
const router = express.Router();
const customersController = require('../controllers/customers.controller')

/* GET users listing. */
router.get('/', customersController.getUsers);

router.get('/:id', customersController.getUser)

router.post('/', customersController.addUser)

router.put('/', customersController.updateUser)

router.delete('/:id', customersController.deleteUser)

module.exports = router;
