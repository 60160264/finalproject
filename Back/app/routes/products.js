const express = require('express');
const router = express.Router();
const productsController = require('../controllers/products.controller')

/* GET users listing. */
router.get('/', productsController.getUsers);

router.get('/:id', productsController.getUser)

router.post('/', productsController.addUser)

router.put('/', productsController.updateUser)

router.delete('/:id', productsController.deleteUser)

module.exports = router;
