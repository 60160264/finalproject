const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: String,
  address: String,
  dob: String,
  gender: String,
  mobile: String,
  email: String
})

module.exports = mongoose.model('Customer', userSchema)