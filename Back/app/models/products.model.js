const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: String,
  price: Number,
  cost: Number
})

module.exports = mongoose.model('Product', userSchema)