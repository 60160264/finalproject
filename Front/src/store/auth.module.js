import AuthService from '../services/auth.service';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },
    register({ commit }, user) {
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess');
          return Promise.resolve(response.data);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      if(state.status.loggedIn) {console.log('Login pass')}
      state.user = user;
      console.log(state.status.loggedIn)
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      if(!state.status.loggedIn) {console.log('Login fail')}
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      if(!state.status.loggedIn) {console.log('Login out')}
      state.user = null;
      console.log(state.status.loggedIn)
    },
    registerSuccess(state) {
      state.status.loggedIn = false;
      if(!state.status.loggedIn) {console.log('register pass')}
    },
    registerFailure(state) {
      state.status.loggedIn = false;
      if(!state.status.loggedIn) {console.log('register fail')}
    }
  }
};